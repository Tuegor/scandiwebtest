<?php
namespace Tuegor;
interface Product {
	public function insert($database);
	public function display();
}
class DVD implements Product {
	private $sku;
	private $name;
	private $price;
	private $size;
	
	public function __construct($sku, $name, $price, $size)
	{
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->size = $size;
	}
	
	public function insert($database)
	{
		$query = "INSERT INTO Products (SKU, Type, Name, Price, Size) VALUES('{$this->sku}', 'D', '{$this->name}', '{$this->price}', '{$this->size}')";
		return $database->query($query);
	}
	
	public function display()
	{
		echo "<div class='item'><input type='checkbox' name='$this->sku'><br>";
		echo '<div class="center">';
		echo "$this->sku<br>";
		echo "$this->name<br>";
		echo "$this->price \$<br>";
		echo "$this->size MiB";
		echo '</div>';
		echo '</div>';
	}
}

class Book implements Product {
	private $sku;
	private $name;
	private $price;
	private $weight;
	
	public function __construct($sku, $name, $price, $weight)
	{
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->weight = $weight;
	}
	
	public function insert($database)
	{
		$query = "INSERT INTO Products (SKU, Type, Name, Price, Weight) VALUES('{$this->sku}', 'B', '{$this->name}', '{$this->price}', '{$this->weight}')";
		return $database->query($query);
	}
	
	public function display()
	{
		echo "<div class='item'><input type='checkbox' name='$this->sku'><br>";
		echo '<div class="center">';
		echo "$this->sku<br>";
		echo "$this->name<br>";
		echo "$this->price \$<br>";
		echo "$this->weight Kg";
		echo '</div>';
		echo '</div>';
	}
}

class Furniture implements Product {
	private $sku;
	private $name;
	private $price;
	private $height;
	private $width;
	private $length;
	
	public function __construct($sku, $name, $price, $height, $width, $length)
	{
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->height = $height;
		$this->width = $width;
		$this->length = $length;
	}
	
	public function insert($database)
	{
		$query = "INSERT INTO Products (SKU, Type, Name, Price, Height, Width, Length) VALUES('{$this->sku}', 'F', '{$this->name}', '{$this->price}', '{$this->height}', '{$this->width}', '{$this->length}')";
		return $database->query($query);
	}
	
	public function display()
	{
		echo "<div class='item'><input type='checkbox' name='$this->sku'><br>";
		echo '<div class="center">';
		echo "$this->sku<br>";
		echo "$this->name<br>";
		echo "$this->price \$<br>";
		echo "$this->height*$this->width*$this->length Cm";
		echo '</div>';
		echo '</div>';
	}
}
class Database
{
	private $host;
	private $user;
	private $password;
	private $database;
	
	private $dbc;
	
	public function __construct()
	{ 
		$this->host = 'localhost';
		$this->user = 'root';
		$this->password = '';
		$this->database = 'scandiwebtest';
	}
	
	public function connect()
	{
		$this->dbc = mysqli_connect($this->host, $this->user, $this->password, $this->database);
		return $this->dbc;
	}
	
	public function connectError()
	{
		echo 'database:'.mysqli_connect_error($this->dbc);
	}
	
	public function query($query)
	{
		$result = mysqli_query($this->dbc, $query);
			echo empty(mysqli_error($this->dbc)) ? '' : 'database:'.mysqli_error($this->dbc);
		return $result;
	}
	
	public function close()
	{
		mysqli_close($this->dbc);
	}
}
?>