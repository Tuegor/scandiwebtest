<!doctype html>
<html>
<head>
<meta charset="utf-8"></meta>
<title>Test</title>
<style>
div.buttons 
{
	float:right;
}
div#container
{
	width:800px;
	margin-left:auto;
	margin-right:auto;
}
div.item
{
	display:inline-block;
	width:20%;
	border:1px solid black;
	margin:15px;
}
div.center
{
	text-align:center;
}
p.small
{
	text-align:center;
}
p.red
{
	color:red;
}
</style>
<script src="list.js">
</script>
</head>
<body>
<form method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
<div class="buttons">
<input type="button" value="Add" id="add"/>
<input type="submit" value="Mass delete"/>
</div>
<h1>Product List</h1>
<hr>
<?php
include ('classes.php');
use Tuegor\Database;
use Tuegor\DVD;
use Tuegor\Book;
use Tuegor\Furniture;

$database = new Database(); 
if($_SERVER['REQUEST_METHOD'] == 'POST')
{	
	//var_dump($_POST);
	$queryDel = 'DELETE FROM Products WHERE SKU IN ("'.implode('","',array_keys($_POST)).'")';
	//var_dump($queryDel);
	if($database->connect())
	{
		$database->query($queryDel);
		$database->close();
	}
	else
		$database->connectError();
}

if($database->connect())
{
	$query = "SELECT * FROM Products";
	$result = $database->query($query);
	echo '<div id="container">';
	while($row = mysqli_fetch_assoc($result))
	{
		$object;
		switch($row['Type'])
		{
			case 'D':
			$object = new DVD($row['SKU'], $row['Name'], $row['Price'], $row['Size']);
			break;
			case 'B':
			$object = new Book($row['SKU'], $row['Name'], $row['Price'], $row['Weight']);
			break;
			case 'F':
			$object = new Furniture($row['SKU'], $row['Name'], $row['Price'], $row['Height'], $row['Width'], $row['Length']);
			break;
		}
		$object -> display();
	}
	echo '</div>';
}
else
	$database->connectError();
$database->close();
?>
</form>
<hr>
<p class="small"><small>Scandiweb test assignment</small></p>
</body>
</html>