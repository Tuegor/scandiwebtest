function onTypeChange()
{
	//console.log("onTypeChange");
	let attributes = document.getElementById("attributes");
	let type = document.getElementsByName("type")[0];
	let innerHTML = "";
	switch(type.selectedOptions[0].value){
		case "dvd":
		innerHTML = `Size(MiB)<input type="text" name="size" value="" size="5"><br>
		Please enter disc size in Mebibytes<br>`;
		break;
		case "book":
		innerHTML = `Weight(Kg)<input type="text" name="weight" value="" size="5"><br>
		Please enter book weight in kilograms<br>`
		break;
		case "furniture":
		innerHTML = `<div class="snp">
		<div class="snprow"><p>Height</p><p><input type="text" name="height" value="" size="5"></p></div>
		<div class="snprow"><p>Width</p><p><input type="text" name="width" value="" size="5"></p></div>
		<div class="snprow"><p>Length</p><p><input type="text" name="length" value="" size="5"></p></div>
		</div>
		Please enter furniture dimensions in Cantimeters<br>`
		break;
	}
	attributes.innerHTML = innerHTML;
}
function onClick()
{
	location.href = "list.php";
}
function onLoad()
{
	let type = document.getElementsByName("type")[0];
	type.onchange = onTypeChange;
	//type.selectedIndex = 0;
	switch(type.selectedOptions[0].value){
		case "dvd":
			if(document.getElementsByName("size").length == 0)
				onTypeChange();
		break;
		case "book":
			if(document.getElementsByName("weight").length == 0)
				onTypeChange();
		break;
		case "furniture":
			if(document.getElementsByName("height").length == 0)
				onTypeChange();
		break;
	}
	
	let cancel = document.getElementById("cancel");
	cancel.onclick = onClick;
}

window.onload = onLoad;
