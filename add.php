<!doctype html>
<html>
<head>
<meta charset="utf-8"></meta>
<title>Test</title>
<style>
div.buttons 
{
	float:right;
}
div.snp
{
	display:table;
}
div.snprow
{
	display:table-row;
}
div.snprow>p
{
	display:table-cell;
}
p.small
{
	text-align:center;
}
p.red
{
	color:red;
}
</style>
<script src="add.js">
</script>
</head>
<body>
<form method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
<div class="buttons">
<input type="submit" value="Save"/>
<input type="button" value="Cancel" id="cancel"/>
</div>
<h1>Product Add</h1>
<hr>
<?php
include ('classes.php'); 
use Tuegor\Database;
use Tuegor\DVD;
use Tuegor\Book;
use Tuegor\Furniture;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{	
	//var_dump($_POST);
	$attributesSet = true;
	foreach ($_POST as $var){
		if(empty($var))
			$attributesSet = false;
	}
	if(!$attributesSet)
		echo '<p class="red">Please submit required data</p>';
	else
	{
		$attributesValid = true;
		foreach ($_POST as $name => $var){
			switch($name){
				case 'sku':
				$attributesValid = $attributesValid && preg_match("/^[a-zA-Z0-9-]{1,20}$/",$var); 
				break;
				case 'name':
				$attributesValid = $attributesValid && preg_match("/^.{1,100}$/",$var); 
				break;
				case 'price':
				$attributesValid = $attributesValid && is_numeric($var);
				break;
				case 'size':
				$attributesValid = $attributesValid && ctype_digit($var);
				break;
				case 'weight':
				$attributesValid = $attributesValid && is_numeric($var);
				break;
				case 'height':
				$attributesValid = $attributesValid && ctype_digit($var);
				break;
				case 'width':
				$attributesValid = $attributesValid && ctype_digit($var);
				break;
				case 'length':
				$attributesValid = $attributesValid && ctype_digit($var);
				break;
			}
		}
		if(!$attributesValid)
			echo '<p class="red">Please provide the data of indicated type</p>';
		else
		{
			$object;
			$database = new Database();
			if($database->connect())
			{
				switch($_POST['type']) {
					case 'dvd': 
					$object = new DVD($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['size']);
					break;
					case 'book':
					$object = new Book($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['weight']);
					break;
					case 'furniture':
					$object = new Furniture($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['height'], $_POST['width'], $_POST['length']);
					break;
				}
				if($object->insert($database))
				{
					$database->close();
					header('Location: list.php');
				}
			}
			else
			{
				$database->connectError();
				$database->close();
			}
		}
	}
}
?>
<div class="snp">
<div class="snprow"><p>SKU</p><p><input type="text" name="sku" value="<?= isset($_POST['sku']) ? $_POST['sku'] : ''?>" size="20"></p></div>
<div class="snprow"><p>Name</p><p><input type="text" name="name" value="<?= isset($_POST['name']) ? $_POST['name'] : ''?>" size="100"></p></div>
<div class="snprow"><p>Price($)</p><p><input type="text" name="price" value="<?= isset($_POST['price']) ? $_POST['price'] : ''?>" size="8"></p></div>
</div>
<br>
Type 
<select name="type">
  <option value="dvd" <?= (isset($_POST['type']) && $_POST['type']=='dvd') || !isset($_POST['type']) ? 'selected' : ''?>>DVD</option>
  <option value="book" <?= (isset($_POST['type']) && $_POST['type']=='book') ? 'selected' : ''?>>Book</option>
  <option value="furniture" <?= (isset($_POST['type']) && $_POST['type']=='furniture') ? 'selected' : ''?>>Furniture</option>
</select>
<br>
<br>
<div id="attributes">
<?php
if(isset($_POST["type"]))
{
	switch($_POST["type"]){
		case 'dvd':
?>
Size(MiB)<input type="text" name="size" value="<?= $_POST['size'] ?>" size="5"><br>
Please enter disc size in Mebibytes<br>
<?php
		break;
		case 'book':
?>
Weight(Kg)<input type="text" name="weight" value="<?= $_POST['weight'] ?>" size="5"><br>
Please enter book weight in kilograms<br>
<?php
		break;
		case 'furniture':
?>
<div class="snp">
	<div class="snprow"><p>Height</p><p><input type="text" name="height" value="<?= $_POST['height'] ?>" size="5"></p></div>
	<div class="snprow"><p>Width</p><p><input type="text" name="width" value="<?= $_POST['width'] ?>" size="5"></p></div>
	<div class="snprow"><p>Length</p><p><input type="text" name="length" value="<?= $_POST['length'] ?>" size="5"></p></div>
</div>
Please enter furniture dimensions in Cantimeters<br>
<?php
		break;
	}
}
else {
?>
Size(MiB)<input type="text" name="size" value="" size="5"><br>
Please enter disc size in Mebibytes<br>
<?php
}
?>
</div>
</form>
<hr>
<p class="small"><small>Scandiweb test assignment</small></p>
</body>
</html>